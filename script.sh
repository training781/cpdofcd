artifactory_url="http://localhost:8081/artifactory"

repo="libs-snapshot-local"
artifactName="CPDOFSEPFIVE"
artifacts="com/agiletestingalliance/cpdofsep/$artifactName"           

url=$artifactory_url/$repo/$artifacts
echo "URL :"+$url
file=`curl -s $url/maven-metadata.xml`

version=`curl -s $url/maven-metadata.xml | grep latest | sed "s/.*<latest>\([^<]*\)<\/latest>.*/\1/"`
echo "Version :"+$version
build=`curl -s $url/$version/maven-metadata.xml | grep '<value>' |head -1 | sed "s/.*<value>\([^<]*\)<\/value>.*/\1/"`
echo "build :"+$build
BUILD_LATEST="$url/$version/$artifactName-$build.war"
echo "File Name  = " +$BUILD_LATEST

echo $BUILD_LATEST > filename.txt
